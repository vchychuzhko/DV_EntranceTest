<?php

class Queries
{
    /**
     * @var PDO $connection
     */
    private static $connection;

    /**
     * @return array
     */
    public function getActorsFrom40To60(): array
    {
        $sql = <<<SQL
            SELECT CONCAT(a.name, " ", a.surname) AS Actor,
              SUM(COALESCE(fe.fee, 0)) AS Fee
            FROM actor AS a
              INNER JOIN film_actor AS fa ON fa.actor_id = a.id
              INNER JOIN fee AS fe ON fe.film_actor_id = fa.id
              INNER JOIN film AS f ON f.id = fa.film_id
            WHERE TIMESTAMPDIFF(YEAR,date_of_birth,CURDATE()) > 40 && TIMESTAMPDIFF(YEAR,date_of_birth,CURDATE()) < 60
            GROUP BY a.id
            ORDER BY Actor
SQL;
        return $this->fetchAll($sql);
    }

    /**
     * @return array
     */
    public function getFeeOfActorByStudio(): array
    {
        $sql = <<<SQL
        SELECT s.name AS StudioName 
            FROM studio AS s
            ORDER BY s.name
SQL;
        return $this->fetchAll($sql);
    }

    /**
     * @return array
     */
    public function getShowFeeOfActorByStudio(): array
    {
        $studioName = $_GET['studio'];
        $sql = <<<SQL
            SELECT s.name AS Studio,
              CONCAT(a.name, " ", a.surname) AS Actor,
              COUNT(f.name) AS "Number of Films"
            FROM studio AS s
              INNER JOIN film AS f ON f.studio_id = s.id
              INNER JOIN film_actor AS fa ON fa.film_id = f.id
              INNER JOIN actor AS a ON a.id = fa.actor_id
            WHERE s.name = :studioName
            GROUP BY a.id, s.id
            ORDER BY Actor
SQL;

        return $this->fetchAll($sql, [':studioName' => $studioName]);//->execute(array('studioName' => $studioName)));
    }

    /**
     * @return array
     */
    public function getActorsWithoutSimilarSurname(): array
    {
        $sql = <<<SQL
            SELECT CONCAT(a.name, " ", a.surname) AS Actor FROM actor a
            LEFT JOIN actor AS b ON a.surname = b.surname AND a.id <> b.id
            WHERE b.id IS NULL
            ORDER BY Actor
SQL;
        return $this->fetchAll($sql);
    }

    /**
     * @return array
     */
    public function getAverageFeesByStudio(): array
    {
        $sql = <<<SQL
            SELECT
              s.name AS Studio,
              CONCAT(a.name, " ", a.surname) AS Actor,
              COUNT(DISTINCT f.id) AS "Number of Films",
              COUNT(fe.id) AS "Number of Fees",
              SUM(COALESCE(fe.fee, 0)) AS "Sum of Fees",
              ROUND(AVG(COALESCE(fe.fee, 0)), 0) AS "Average Fee"
            FROM studio AS s
              INNER JOIN film AS f ON f.studio_id = s.id
              INNER JOIN film_actor AS fa ON fa.film_id = f.id
              INNER JOIN actor AS a ON a.id = fa.actor_id
              INNER JOIN fee AS fe ON fe.film_actor_id = fa.id
            WHERE (YEAR(CURDATE()) - f.year) <= 10
            GROUP BY s.id, a.id
            ORDER BY AVG(fe.fee) DESC 
SQL;
        return $this->fetchAll($sql);
    }

    /**
     * @return PDO
     */
    private function getConnection(): PDO
    {
        if (null === self::$connection) {
            /* Connect to a MySQL database using driver invocation */
            $dsn = 'mysql:dbname=task_3_mysql;host=127.0.0.1';
            $user = 'debian-sys-maint';
            $password = 'HxNL3LjhGVAY5qzl';

            try {
                self::$connection = new PDO($dsn, $user, $password);
                self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                // echo 'Connection failed: ' . $e->getMessage() . "\n";
                throw $e;
            }
        }

        return self::$connection;
    }

    /**
     * @param string $sql
     * @param array<key:string, value:mixed> $params
     * @return array
     */
    private function fetchAll(string $sql, array $params = []): array
    {
        $data = [];

        try {
            $statement = $this->getConnection()->prepare($sql);

            foreach ($params as $key => $value) {
                $statement->bindValue($key, $value);
            }
            $statement->execute();
            $data = $statement->fetchAll() ?: [];
        } catch (PDOException $e) {
            // write data to the error stream or to the log file
            // echo 'Database error: ' . $e->getMessage() . "\n";
            // throw $e;
        }

        return $data;
    }
}