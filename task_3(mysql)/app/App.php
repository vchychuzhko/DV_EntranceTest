<?php
require_once 'Queries.php';

class App
{
    public function run(): void
    {
//        echo "<pre>";
//        var_dump($_SERVER);
//        exit;

        try {
            ob_start();
            include 'templates/layout.phtml';
            $response = ob_get_clean();
            // set and send some response headers here
            echo $response;
        } catch (Exception $e) {
            echo $e->getMessage();
            echo $e->getTraceAsString();
        }
    }

    public function renderContent(): void
    {
        $queries = new Queries();
        $uri = strtok(trim($_SERVER['REQUEST_URI'], '/'), '?');
        $template = 'templates/' . $uri . '.phtml';

        try {
            $data = $queries->{$this->queryToMethodName($uri)}();
        } catch (Throwable $t) {
            if(empty($uri)){
                $template = 'templates/home.phtml';
            } else {
                $template = 'templates/404.phtml';
            }
        } finally {
            include $template;
        }
    }

    /**
     * @param string $uri
     * @return string
     */
    private function queryToMethodName(string $uri): string
    {
            return 'get' . $this->dashesToCamelCase($uri);
    }

    /**
     * @param string $string
     * @param bool $capitalizeFirstCharacter
     * @return string
     */
    private function dashesToCamelCase(string $string, $capitalizeFirstCharacter = true): string
    {
        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }

        return $str;
    }
}