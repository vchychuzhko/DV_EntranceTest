SELECT @studio_name := "Warner Bros.";

SELECT s.name AS Studio,
  CONCAT(a.name, " ", a.surname) AS Actor,
  COUNT(f.name) AS "Number of Films"
FROM studio s
  INNER JOIN film f ON f.studio_id = s.id
  INNER JOIN film_actor fa ON fa.film_id = f.id
  INNER JOIN actor a ON a.id = fa.actor_id
WHERE s.name = @studio_name
GROUP BY a.id, s.id
ORDER BY s.name
