CREATE TABLE task_3_mysql.actor
(
    id int(10) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(50) NOT NULL,
    surname varchar(50) NOT NULL,
    date_of_birth date NOT NULL
);
INSERT INTO task_3_mysql.actor (id, name, surname, date_of_birth) VALUES (1, 'Leonardo', 'DiCaprio', '1974-11-11');
INSERT INTO task_3_mysql.actor (id, name, surname, date_of_birth) VALUES (2, 'Brad', 'Pitt', '1963-12-18');
INSERT INTO task_3_mysql.actor (id, name, surname, date_of_birth) VALUES (3, 'Edward ', 'Norton', '1969-08-18');
INSERT INTO task_3_mysql.actor (id, name, surname, date_of_birth) VALUES (4, 'Helena', 'Bonham Carter', '1966-05-26');
INSERT INTO task_3_mysql.actor (id, name, surname, date_of_birth) VALUES (5, 'Matthew', 'McConaughey', '1969-11-04');
INSERT INTO task_3_mysql.actor (id, name, surname, date_of_birth) VALUES (6, 'Hugh', 'Jackman', '1968-10-12');
INSERT INTO task_3_mysql.actor (id, name, surname, date_of_birth) VALUES (7, 'Cristian', 'Bale', '1974-01-30');
INSERT INTO task_3_mysql.actor (id, name, surname, date_of_birth) VALUES (8, 'Will', 'Smith', '1968-09-25');
INSERT INTO task_3_mysql.actor (id, name, surname, date_of_birth) VALUES (9, 'Margot', 'Robbie', '1990-07-02');
INSERT INTO task_3_mysql.actor (id, name, surname, date_of_birth) VALUES (10, 'Maggie', 'Gyllenhaal', '1977-11-16');
INSERT INTO task_3_mysql.actor (id, name, surname, date_of_birth) VALUES (11, 'Jake', 'Gyllenhaal', '1980-12-19');
INSERT INTO task_3_mysql.actor (id, name, surname, date_of_birth) VALUES (12, 'Jaden', 'Smith', '1998-07-08');
INSERT INTO task_3_mysql.actor (id, name, surname, date_of_birth) VALUES (13, 'Kate', 'Winslet', '1975-10-05');
INSERT INTO task_3_mysql.actor (id, name, surname, date_of_birth) VALUES (14, 'Michael', 'Caine', '1933-03-14');
CREATE TABLE task_3_mysql.fee
(
    id int(10) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    film_actor_id int(10) NOT NULL,
    fee int(10),
    CONSTRAINT FK__film_actor FOREIGN KEY (film_actor_id) REFERENCES task_3_mysql.film_actor (id)
);
CREATE INDEX FK__film_actor ON task_3_mysql.fee (film_actor_id);
INSERT INTO task_3_mysql.fee (id, film_actor_id, fee) VALUES (1, 1, 10000000);
INSERT INTO task_3_mysql.fee (id, film_actor_id, fee) VALUES (2, 2, 11000000);
INSERT INTO task_3_mysql.fee (id, film_actor_id, fee) VALUES (3, 1, 100);
INSERT INTO task_3_mysql.fee (id, film_actor_id, fee) VALUES (4, 19, 5000000);
INSERT INTO task_3_mysql.fee (id, film_actor_id, fee) VALUES (5, 22, 3500000);
INSERT INTO task_3_mysql.fee (id, film_actor_id, fee) VALUES (6, 32, 5600000);
INSERT INTO task_3_mysql.fee (id, film_actor_id, fee) VALUES (7, 32, 4400000);
INSERT INTO task_3_mysql.fee (id, film_actor_id, fee) VALUES (8, 15, 2000000);
INSERT INTO task_3_mysql.fee (id, film_actor_id, fee) VALUES (9, 16, 1500000);
INSERT INTO task_3_mysql.fee (id, film_actor_id, fee) VALUES (10, 16, 2300000);
INSERT INTO task_3_mysql.fee (id, film_actor_id, fee) VALUES (11, 27, 1500000);
INSERT INTO task_3_mysql.fee (id, film_actor_id, fee) VALUES (12, 31, 30000000);
INSERT INTO task_3_mysql.fee (id, film_actor_id, fee) VALUES (13, 20, null);
INSERT INTO task_3_mysql.fee (id, film_actor_id, fee) VALUES (14, 7, null);
CREATE TABLE task_3_mysql.film
(
    id int(10) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(150) NOT NULL,
    director varchar(50) NOT NULL,
    year year(4) NOT NULL,
    budget int(10) NOT NULL,
    box_office bigint(10) NOT NULL,
    duration time NOT NULL,
    genre varchar(50) NOT NULL,
    studio_id int(10) NOT NULL
);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (1, 'Se7en', 'David Fincher', 1995, 33000000, 327311859, '02:07:00', 'thriller', 6);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (2, 'Titanic', 'James Cameron', 1997, 200000000, 2185372302, '03:14:00', 'drama', 1);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (3, 'Fight Club', 'David Fincher', 1999, 63000000, 100853753, '02:11:00', 'thriller', 1);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (4, 'Ocean''s Eleven', 'Steven Soderbergh', 2001, 85000000, 450717150, '01:56:00', 'criminal', 2);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (5, 'The Prestige', 'Christopher Nolan', 2006, 40000000, 109676311, '02:05:00', 'thriller', 2);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (6, 'I Am Legend', 'Francis Lawrence', 2007, 150000000, 585349010, '01:36:00', 'horror', 2);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (7, 'The Dark Knight', 'Christopher Nolan', 2008, 185000000, 1004558444, '02:32:00', 'thriller', 2);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (8, 'Inception', 'Christopher Nolan', 2010, 160000000, 825532764, '02:28:00', 'thriller', 2);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (9, 'Nightcrawler', 'Dan Gilroy', 2013, 8500000, 38697217, '01:58:00', 'criminal', 9);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (10, 'Prisoners', 'Denis Villeneuve', 2013, 46000000, 122126687, '02:33:00', 'thriller', 8);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (11, 'After Earth', 'M. Night Shyamalan', 2013, 130000000, 245055287, '01:40:00', 'fantasy', 3);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (12, 'Fury', 'David Ayer', 2014, 68000000, 211817906, '02:14:00', 'war film', 3);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (13, 'Focus', 'Glenn Ficarra', 2014, 50100000, 159062963, '01:44:00', 'drama', 2);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (14, 'Interstellar', 'Christopher Nolan', 2014, 165000000, 675120017, '02:49:00', 'drama', 2);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (15, 'Demolition', 'Jean-Marc Vallée', 2015, 10000000, 2187958, '01:41:00', 'drama', 7);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (16, 'Collateral Beauty', 'David Frankel', 2016, 36000000, 88528280, '01:38:00', 'drama', 2);
INSERT INTO task_3_mysql.film (id, name, director, year, budget, box_office, duration, genre, studio_id) VALUES (17, 'Suicide Squad', 'David Ayer', 2016, 175000000, 746846894, '01:58:00', 'fantasy', 2);
CREATE TABLE task_3_mysql.film_actor
(
    id int(10) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    film_id int(10) NOT NULL,
    actor_id int(10) NOT NULL,
    CONSTRAINT FK_film_actor_film FOREIGN KEY (film_id) REFERENCES task_3_mysql.film (id),
    CONSTRAINT FK_film_actor_actor FOREIGN KEY (actor_id) REFERENCES task_3_mysql.actor (id)
);
CREATE INDEX FK_film_actor_film ON task_3_mysql.film_actor (film_id);
CREATE INDEX FK_film_actor_actor ON task_3_mysql.film_actor (actor_id);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (1, 1, 2);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (2, 2, 1);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (3, 2, 13);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (4, 3, 2);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (5, 3, 3);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (6, 4, 2);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (7, 5, 7);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (8, 5, 6);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (9, 5, 14);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (10, 6, 8);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (11, 7, 7);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (12, 7, 14);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (13, 8, 1);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (14, 8, 14);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (15, 9, 11);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (16, 10, 11);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (17, 10, 6);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (18, 11, 8);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (19, 11, 12);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (20, 12, 2);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (21, 13, 8);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (22, 13, 9);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (23, 3, 4);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (24, 7, 10);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (25, 14, 5);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (26, 14, 14);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (27, 15, 11);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (28, 16, 8);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (29, 16, 3);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (30, 16, 13);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (31, 17, 8);
INSERT INTO task_3_mysql.film_actor (id, film_id, actor_id) VALUES (32, 17, 9);
CREATE TABLE task_3_mysql.studio
(
    id int(10) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(100) NOT NULL,
    foundation_year year(4) NOT NULL
);
INSERT INTO task_3_mysql.studio (id, name, foundation_year) VALUES (1, '20th Century Fox', 1935);
INSERT INTO task_3_mysql.studio (id, name, foundation_year) VALUES (2, 'Warner Bros.', 1923);
INSERT INTO task_3_mysql.studio (id, name, foundation_year) VALUES (3, 'Columbia Pictures', 1919);
INSERT INTO task_3_mysql.studio (id, name, foundation_year) VALUES (4, 'Paramount Pictures', 1912);
INSERT INTO task_3_mysql.studio (id, name, foundation_year) VALUES (5, 'Universal Studios', 1912);
INSERT INTO task_3_mysql.studio (id, name, foundation_year) VALUES (6, 'New Line Cinema', 1967);
INSERT INTO task_3_mysql.studio (id, name, foundation_year) VALUES (7, 'Black Label Media', 2013);
INSERT INTO task_3_mysql.studio (id, name, foundation_year) VALUES (8, 'Alcon Entertainment', 1997);
INSERT INTO task_3_mysql.studio (id, name, foundation_year) VALUES (9, 'Bold Films', 2004);