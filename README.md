# Entrance test
###### README.md

Here is template of UIBrush site.<br />
To launch this, input in your CLI: 
```
npm start
```

## Task 2
Colored blocks are moving by your click. They are placed by heptagonal star.<br />
Open [index.html](/task_2(jquery_carousel)/index.html) in browser.
Files are [here](/task_2(jquery_carousel)).

## Task 3
Film & Actor database.<br />
To launch this you have to setup MySql.<br />
Database files are [here](/task_3(mysql)).
