;(function ($) {
    $.widget('vlad.slider', {
        options: {
            animationSpeed: 2000,
            stepDelay: 2000,
            width: 0,
            numberOfSlides: 0,
            currentImageIndex: 1
        },

        autoSlideInterval: 0,

        _create: function () {
            if(this.options.width === 0) {
                this.options.width = $( window ).width();
            }

            let $slidesWrapper = this.element.find('.slides'),
                $images = $slidesWrapper.find('> *'),
                firstSlide = $images.first().clone(),
                sliderWidth = this.options.width;

            if(this.options.numberOfSlides === 0) {
                this.options.numberOfSlides = $images.length;
            }

            //initialization of size and position
            $slidesWrapper.css({
                left: (-sliderWidth) + 'px'
            });

            $slidesWrapper.prepend($images.last().clone());
            $slidesWrapper.append(firstSlide);
            $images = $slidesWrapper.find('> *');

            $images.css({
                width: sliderWidth + 'px'
            });

            //resize
            $( window ).resize(this.resize.bind(this));

            //binding
            this.bindClickListeners();
            // this.autoSlide();

            // // autoSlide
            // this.autoSlide = setInterval(function () {
            //     this.element.find('.next').triggerHandler('click');
            //     // this.move({
            //     //     currentTarget: this.element.find('.next')
            //     // });    //how to call it???
            // }.bind(this), this.options.stepDelay);

            //click
            // this.element.find('.previous, .next').on('click', this.move.bind(this));
        },

        bindClickListeners: function() {
            this.element.find('.previous, .next').on('click', this.move.bind(this));
            this.autoSlide();
            // setTimeout(function (){
            //         console.log('wait');
            //         this.autoSlide()
            //     }.bind(this),
            //     this.options.waitingTime);
        },

        unbindClickListeners: function() {
            this.element.find('.previous, .next').off('click');
            clearInterval(this.autoSlideInterval);
        },

        autoSlide: function() {
            this.autoSlideInterval = setInterval(function () {
                this.element.find('.next').triggerHandler('click');
            }.bind(this), this.options.stepDelay);
        },

        move: function (event) {
            this.unbindClickListeners();
            let direction = $(event.currentTarget).hasClass('previous') ? -1 : 1,
                duration = this.options.animationSpeed,
                $slides = this.element.find('.slides'),
                // $navigationButtons = this.element.find('.previous, .next'),
                sliderCurrentPosition = parseInt($slides.position().left),
                numberOfSlides = this.options.numberOfSlides,
                sliderWidth = this.options.width,
                currentSlideIndex = this.options.currentImageIndex - 1,
                cycle = false;

            // debugger;
            // $navigationButtons.addClass('disabled');

            currentSlideIndex += direction;
            cycle = (currentSlideIndex === -1 || currentSlideIndex === numberOfSlides);

            $slides.css({
                transition: 'all ' + duration + 'ms',
                left: (sliderCurrentPosition - direction * sliderWidth) + 'px'
            });

            setTimeout(function () {
                if (cycle) {
                    currentSlideIndex = (currentSlideIndex === -1) ? numberOfSlides - 1 : 0;
                    $slides.css({
                        left: -(1 + currentSlideIndex) * sliderWidth + 'px',
                        transitionDuration: '0ms'
                    })
                }
                // $navigationButtons.removeClass('disabled');
                this.options.currentImageIndex = currentSlideIndex + 1;
                this.bindClickListeners();
            }.bind(this), duration);
        },

        resize: function () {
            let $slidesWrapper = this.element.find('.slides'),
                $images = $slidesWrapper.find('> *'),
                sliderWidth = this.options.width;

            this.options.width = $( window ).width();

            $images.css({
                width: sliderWidth + 'px'
            });

            $slidesWrapper.css({
                left: (-this.options.currentImageIndex * sliderWidth) + 'px',
                transitionDuration: '0ms'
            });
        }
    });
})(jQuery);

// $(document).ready(function(){
//     $(".next").click(function(){
//         var width = $( window ).width();
//         var currImage = $(".img.curr");
//         var currImageIndex = $(".img.curr").index();
//         var nextImageIndex = currImageIndex + 1;
//         if(nextImageIndex == ($(".img:last").index() + 1)){
//             var nextImage = $(".img").eq(0);
//         } else {
//             var nextImage = $(".img").eq(nextImageIndex);
//         }
//
//         nextImage.css({"display": "block", "left": width + "px"});
//         nextImage.animate({"left": 0}, 500);
//         nextImage.addClass("curr");
//
//         currImage.fadeOut(1000);
//         currImage.removeClass("curr");
//     });
//
//     $(".prev").click(function(){
//         var width = $( window ).width();
//         var currImage = $(".img.curr");
//         var currImageIndex = $(".img.curr").index();
//         var prevImageIndex = currImageIndex - 1;
//         var prevImage = $(".img").eq(prevImageIndex);
//         var prevImageWidth = prevImage.width();
//
//         prevImage.css({"display": "block", "left": 0 - prevImageWidth + "px"});
//         prevImage.animate({"right": width + "px", "left": 0 - (prevImageWidth - width) + "px"}, 500);
//         prevImage.addClass("curr");
//
//         currImage.fadeOut(1000);
//         currImage.removeClass("curr");
//     });
// });
