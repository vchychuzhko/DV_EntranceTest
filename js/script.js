top_navigation_button.onclick = function openMenu() {
    var x = document.getElementById("top_navigation");
    var b = document.getElementById("top_navigation_button");

    if(x.className === "topnav") {
        x.className += " open";
        b.text = "X";
    } else {
        x.className = "topnav";
        b.text = "☰";
    }
};

$(document).ready(function(){
    $(".next").click(function(){
        var width = $( window ).width();
        var currImage = $(".img.curr");
        var currImageIndex = $(".img.curr").index();
        var nextImageIndex = currImageIndex + 1;
        if(nextImageIndex == ($(".img:last").index() + 1)){
            var nextImage = $(".img").eq(0);
        } else {
            var nextImage = $(".img").eq(nextImageIndex);
        }

        nextImage.css({"display": "block", "left": width + "px"});
        nextImage.animate({"left": 0}, 500);
        nextImage.addClass("curr");

        currImage.fadeOut(1000);
        currImage.removeClass("curr");
    });

    $(".prev").click(function(){
        var width = $( window ).width();
        var currImage = $(".img.curr");
        var currImageIndex = $(".img.curr").index();
        var prevImageIndex = currImageIndex - 1;
        var prevImage = $(".img").eq(prevImageIndex);
        var prevImageWidth = prevImage.width();

        prevImage.css({"display": "block", "left": 0 - prevImageWidth + "px"});
        prevImage.animate({"right": width + "px", "left": 0 - (prevImageWidth - width) + "px"}, 500);
        prevImage.addClass("curr");

        currImage.fadeOut(1000);
        currImage.removeClass("curr");
    });
});
